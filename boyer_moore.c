/*
 * boyer-moore 算法
 * http://www.ruanyifeng.com/blog/2013/05/boyer-moore_string_search_algorithm.html
 * ‘位置’指的是字符在搜索串(pattern)中的位置（index从0开始）
*/
#include<stdio.h>
#include<string.h>

#define MAX_LENGTH 1024
#define PATTERN_MAX_LENGTH 126

int main(){

    char str[MAX_LENGTH];
    fgets(str , MAX_LENGTH , stdin);

    char pattern[PATTERN_MAX_LENGTH];
    fgets(pattern , PATTERN_MAX_LENGTH , stdin);

    int str_len = strlen(str) - 1,
        p_len   = strlen(pattern) - 1;
    
    int i,j,k;
    //坏字符后移位数 = 坏字符的位置 - 搜索词中的上一次出现位置
    
    for(i=0 ; i<str_len ; ){
        for(j=p_len-1 ; j>=0 ; j--){
            if(str[i + j] != pattern[j]){
                break;
            }
        }

        if(j<0){
            printf("index: %d \n",i);
            i += p_len;
        }
        else{
            for(k=j-1 ; k>=0 ; k--){
                if(pattern[k] == str[i + j]){
                    break;
                }
            }
                        
            i += j - ( k<0? -1: k);
        }

    }


    return 0;
}