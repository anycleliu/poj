#include<stdio.h>
#include<stdlib.h>

int main(){
    //freopen("poj_1028.txt","r",stdin);

    int n;
    int **m;
    scanf("%d",&n);

    m=(int**)malloc(3*sizeof(int*));
    for(int i=0;i<3;i++){
        m[i]=(int*)malloc(n*sizeof(int));        
    }
    
    int m0=0,m1=0,m2=0;
    for(int i=0;i<n;i++){
        scanf("%d %d %d",&m[0][i],&m[1][i],&m[2][i]);
    }

    int i,j,sn=0,s;
    for(i=0;i<n;i++){
        s=1;
        for(j=0;j<n;j++){
            if(i==j) continue;
            if(m[0][j]<m[0][i] && m[1][j]<m[1][i] && m[2][j]<m[2][i]){
                s=0;
                break;
            }
        }
        if(s==1){
            sn++;
        }
    }

    printf("%d\n",sn);

    return 0;
}