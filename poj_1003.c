#include<stdio.h>
#include<stdlib.h>
/*
 * 1. 找到速度为0的点的位置
 * 2. 对于速度为0的位置，左边方向向左的蚂蚁、右边方向向右的蚂蚁 忽略，因为不会影响到静止的蚂蚁
 * 3. 从静止的蚂蚁像左右遍历，找到一对蚂蚁就抵消
 * 4. 统计剩下的蚂蚁，那边留下的蚂蚁多，就取那边最靠近静止的蚂蚁的那个蚂蚁，掉下去的时间
*/
int main(){
    //freopen("poj_1003.txt","r",stdin);

    int m,p,am;
    int a[100];
    for(int i=0;i<100;i++) a[i]=-2;

    scanf("%d",&m);
    for(int i=0;i<m;i++){
        scanf("%d",&p);
        scanf("%d",a+p);
        if((*(a+p))==0){
            am=p;
        }
    }

    int l=am-1 , r=am+1;
    int ln=0,rn=0;

    for(l ; l>0 ; l--){
        if(a[l] == -2) continue;
        else if(a[l] == -1){
            a[l]=-2;
        }
        else{
            ln++;
        }
    }

    for(r ; r<100 ; r++){
        if(a[r] == -2) continue;
        else if(a[r] == 1){
            a[r]=-2;
        }
        else{
            rn++;
        }
    }

    l=am-1;
    r=am+1;
    while(1){
        while(l>0 && a[l]==-2) l--;
        if(l<=0) break;

        while(r<100 && a[r]==-2) r++;
        if(r>=100) break;

        a[l]=a[r]=-2;
    }

    if(ln==rn) printf("Cannot fall!\n");
    else if(ln>rn){
        for(int i=am-1;i>0;i--){
            if(a[i]!=-2){
                printf("%d\n",100-i);
                break;
            }
        }
    }
    else{
        for(int i=am+1 ; i<100 ; i++){
            if(a[i]!=-2){
                printf("%d\n",i);
                break;
            }
        }
    }

    return 0;
}
