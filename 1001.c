#include<stdio.h>
#include<math.h>

double power(double f,int n){
    if(n==0) return 1;
    if(n%2==0){
        return power(f,n/2)*power(f,n/2);
    }
    else{
        return f*power(f,(n-1)/2)*power(f,(n-1)/2);
    }
}

int main(){
    float a;
    int b;

    while(scanf("%f %d",&a,&b)!=EOF){
        printf("%lf",power((double)a,b));
    }
    
    return 0;
}